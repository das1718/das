/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.db;

import db.model.UserEntity;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.h2.jdbc.JdbcSQLException;

/**
 *
 * @author sofib
 */
public class CRUDUser {
   private static final String TAG = CRUDUser.class.getSimpleName();

    private static final String TABLE_NAME = "users";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";
    
    UserEntity userEntity = new UserEntity();
    

    public boolean addUser(String name) throws ClassNotFoundException {

        if (getProfileByName(name) == null) {
            String command
                    = "INSERT INTO " + TABLE_NAME + " (" + COLUMN_NAME + ") " 
                    + "VALUES ('" + name + "')";

            System.out.println("[command] :" + command);

            try {
                Statement statement = Session.getSession().getConnection().createStatement();
                int result = statement.executeUpdate(command);

                return result != 0;
            } catch (SQLException ex) {
                Logger.getLogger(TAG).log(Level.SEVERE, null, ex);
            }
        } else {
            System.err.println("The username " + name + " already exists on DB.");
        }

        return false;
    }
    
    public String getProfileByName(String name) throws ClassNotFoundException {
        ResultSet result = null;
        String foundName = null;
        try {
            Statement statement = Session.getSession().getConnection().createStatement();
            result = statement.executeQuery("select " + "*" + " from " + TABLE_NAME + " WHERE " + COLUMN_NAME + " = '" + name + "'");
            result.next();
            foundName = result.getString(("name"));
            System.out.println(foundName);
            result.close();
            
        } catch (SQLException ex) {
            System.out.println("Nao encontrou dados!");
            Logger.getLogger(TAG).log(Level.SEVERE, null, ex);
            return null;
        }
        return foundName;
 
    }
    
    public UserEntity convertResultSetToUserEntity(ResultSet resultSet) {
        
        if (resultSet != null) {
            try {
                userEntity.setId(resultSet.getInt(COLUMN_ID));
                userEntity.setName(resultSet.getString(COLUMN_NAME));
                return userEntity;
            } catch (SQLException ex) {
                Logger.getLogger(TAG).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
}
