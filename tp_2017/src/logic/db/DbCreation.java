/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.db;

import java.sql.*;

/**
 *
 * @author sofib
 */
public class DbCreation {
    public void createTables() throws ClassNotFoundException, SQLException {
        Statement statement = Session.getSession().getConnection().createStatement();
        statement.setQueryTimeout(30);
        statement.executeUpdate("drop table if exists users");
        statement.executeUpdate("drop table if exists files");
        String query = "create table users (id integer auto_increment, " +
             "name varchar(255))";
        statement.executeUpdate(query);
        query = "create table files (id integer auto_increment, " +
             "userID integer, fileName varchar(255))";
        statement.executeUpdate(query);   
    }
}
