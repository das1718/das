/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author sofib
 */
public class Session {
    private static Session session = null;
    
    public static Session getSession() throws ClassNotFoundException, SQLException {
        if(session == null) {
            session = new Session();
        }
        return session;
    }
    
    public Connection getConnection() throws ClassNotFoundException, SQLException {
        Connection connection=null;
        Class.forName("org.h2.Driver");
        connection = DriverManager.
        getConnection("jdbc:h2:~/test", "sa", ""); //user:sa pass:"" 
        return connection;
    }
}
